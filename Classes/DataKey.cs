using DataCore.DataSystem;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataCore.Classes
{
    public static class DataExtension
    {
        public static T Get<T>(this Dictionary<DataKey, DataStorable> data, DataKey<T> key) where T : DataStorable
        {
            return (T)data[key.Erase()];
        }
        public static async Task<T> Get<T>(this Task<Dictionary<DataKey, DataStorable>> data, DataKey<T> key) where T : DataStorable
        {
            return (T)(await data)[key.Erase()];
        }
    }

    public class DataKeyEqualityComparer : EqualityComparer<DataKey>
    {
        public override bool Equals(DataKey x, DataKey y)
        {
            return x.DataType == y.DataType && x.Keys.Equals(y.Keys);
        }

        public override int GetHashCode(DataKey obj)
        {
            return obj.GetHashCode();
        }
    }

    /// <summary>
    /// A key specifically referring to a typed object stored in the database.
    /// </summary>
    /// <typeparam name="T">The related type.</typeparam>
    public class DataKey<T>
    {
        /// <summary>
        /// The reference data used to build the implementation specific key.
        /// Multiple parts allows for more control over building the key.
        /// </summary>
        public readonly String[] Keys;
        /// <summary>
        /// The type we are referring to.
        /// </summary>
        public Type DataType => typeof(T);

        public DataKey(params String[] keys)
        {
            Keys = keys;
        }

        public override String ToString()
        {
            return $"{typeof(T)}:{String.Join('&', Keys)}";
        }

        public static implicit operator DataKey(DataKey<T> operand)
        {
            return new DataKey(operand.Keys, typeof(T));
        }

        /// <summary>
        /// Convert to an untyped key.
        /// We still hold a reference to the key, but we use reflection to get the type.
        /// </summary>
        /// <returns>The equivalent key, with a non-generic reference to the type.</returns>
        public DataKey Erase()
        {
            return new DataKey(Keys, DataType);
        }
    }

    /// <summary>
    /// A key specifically referring to a typed object stored in the database.
    /// The type is stored as a field, as so has to be interacted with using reflection.
    /// </summary>
    public class DataKey
    {
        /// <summary>
        /// The reference data used to build the implementation specific key.
        /// Multiple parts allows for more control over building the key.
        /// </summary>
        public readonly String[] Keys;
        /// <summary>
        /// The type we are referring to.
        /// </summary>
        public Type DataType;

        public DataKey(String[] keys, Type type)
        {
            Keys = keys;
            DataType = type;
        }

        public override String ToString()
        {
            return $"{DataType}:{String.Join('&', Keys)}";
        }

        public override int GetHashCode()
        {
            String code = ToString();
            return code.GetHashCode();
        }
    }
}
