using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;

namespace DataCore.Classes
{
    /// <summary>
    /// A database that uses a local dictionary in memory.
    /// Very basic. Data is not stored to disk, so the database is wiped on restart.
    /// Useful for throwaway unit tests.
    /// </summary>
    public class LocalLoader : ILoadable
    {
        /// <summary>
        /// The storage.
        /// </summary>
        private readonly Dictionary<Type, Dictionary<String, DataStorable>> _localCache = new Dictionary<Type, Dictionary<String, DataStorable>>();

        public async Task<T> Interact<T>(Func<ITransaction, Task<T>> interaction)
        {
            var trans = new LocalTransaction(_localCache);
            T result = await interaction(trans);
            trans.BatchComplete();
            return result;
        }

        public async Task Interact(Func<ITransaction, Task> interaction)
        {
            var trans = new LocalTransaction(_localCache);
            await interaction(trans);
            trans.BatchComplete();
        }

        public async Task<T> Gather<T>(Func<ITransaction, Task<T>> interaction)
        {
            var trans = new LocalTransaction(_localCache);
            return await interaction(trans);
        }
    }
}