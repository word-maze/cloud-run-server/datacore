﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Interfaces;

namespace DataCore.Classes
{
    /// <summary>
    /// A transaction for writing to temporary storage.
    /// </summary>
    public class LocalTransaction : ITransaction
    {
        private readonly Dictionary<Type, Dictionary<String, DataStorable>> _cache;
        private readonly Dictionary<DataKey, DataStorable> _dataToStore;
        private readonly Dictionary<DataKey, DataStorable> _dataToDelete;

        public LocalTransaction(Dictionary<Type, Dictionary<String, DataStorable>> cache)
        {
            _cache = cache;
            _dataToStore = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());
            _dataToDelete = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());
        }

        public async Task<DataStorable> Get(DataKey key)
        {
            if (!_cache.ContainsKey(key.DataType) || !_cache[key.DataType].ContainsKey(key.ToString()))
            {
                return null;
            }

            return (DataStorable)Activator.CreateInstance(key.DataType, _cache[key.DataType][key.ToString()]);
        }

        public async Task<T> Get<T>(DataKey<T> key) where T : DataStorable, new()
        {
            if (!_cache.ContainsKey(typeof(T)) || !_cache[typeof(T)].ContainsKey(key.ToString()))
            {
                return null;
            }

            return (T)_cache[typeof(T)][key.ToString()];
        }

        public async Task<List<T>> Query<T>(List<QueryFilter> parameters) where T : DataStorable, new()
        {
            if (!_cache.ContainsKey(typeof(T)))
            {
                return new List<T>();
            }

            var results = _cache[typeof(T)].ToArray();

            foreach ((String attribute, FilterType operation, Object value) in parameters)
            {

                var field = typeof(T).GetField(attribute);
                switch (operation)
                {
                    case FilterType.EQUALS:
                    {
                        var temp = results.Where(x => field.GetValue(x.Value).Equals(value));
                        results = temp.ToArray();
                        break;
                    }
                    case FilterType.LESS_THAN:
                    {
                        results = results.Where(x => ((IComparable)field.GetValue(x.Value)).CompareTo(value) < 0).ToArray();
                            break;
                    }
                    case FilterType.LESS_THAN_OR_EQUAL_TO:
                    {
                        results = results.Where(x => ((IComparable)field.GetValue(x.Value)).CompareTo(value) <= 0).ToArray();
                            break;
                    }
                    case FilterType.GREATER_THAN:
                    {
                        results = results.Where(x => ((IComparable)field.GetValue(x.Value)).CompareTo(value) > 0).ToArray();
                            break;
                    }
                    case FilterType.GREATER_THAN_OR_EQUALS_TO:
                    {
                        results = results.Where(x => ((IComparable)field.GetValue(x.Value)).CompareTo(value) >= 0).ToArray();
                            break;
                    }
                }
            }

            return results.Select(x => (T)x.Value).ToList();
        }

        public async Task<Dictionary<DataKey, DataStorable>> Get(params DataKey[] keys)
        {
            var docs = keys.Select(key =>
            {
                _cache.TryGetValue(key.DataType, out Dictionary<string, DataStorable> typeMap);
                if (typeMap == null)
                {
                    return null;
                }

                typeMap.TryGetValue(key.ToString(), out DataStorable data);
                return data;

            }).ToList();

            var result = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());

            for (var i = 0; i < keys.Length; i++)
            {
                result.Add(keys[i], docs[i]);
            }

            return result;
        }

        public void QueueSet(params DataStorable[] data)
        {
            QueueSet(data.AsEnumerable());
        }

        public void QueueSet(IEnumerable<DataStorable> data)
        {
            foreach (var dataStorable in data)
            {
                if (_dataToStore.ContainsKey(dataStorable.Key))
                {
                    _dataToStore[dataStorable.Key] = dataStorable;
                }
                else
                {
                    _dataToStore.Add(dataStorable.Key, dataStorable);
                }
            }
        }

        public void QueueDelete(params DataStorable[] data)
        {
            QueueDelete(data.AsEnumerable());
        }

        public void QueueDelete(IEnumerable<DataStorable> data)
        {
            foreach (var dataStorable in data)
            {
                _dataToDelete.Add(dataStorable.Key, dataStorable);
            }
        }

        public void BatchComplete()
        {
            foreach (var dataStorable in _dataToDelete.Values)
            {
                Delete(dataStorable);
            }
            foreach (var dataStorable in _dataToStore.Values)
            {
                Set(dataStorable);
            }
        }

        /// <summary>
        /// Store an object to memory. This is done internally, and should not be called as part of a transaction.
        /// </summary>
        /// <param name="entity"></param>
        public async void Set(DataStorable entity)
        {
            if (!_cache.ContainsKey(entity.GetType()))
            {
                _cache.Add(entity.GetType(), new Dictionary<String, DataStorable>());
            }

            _cache[entity.GetType()][entity.Key.ToString()] = entity;
        }

        public async void Delete(DataStorable entity)
        {
            if (_cache.ContainsKey(entity.GetType()))
            {
                _cache[entity.GetType()].Remove(entity.Key.ToString());
            }
        }
    }
}
