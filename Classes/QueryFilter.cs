﻿using System;

namespace DataCore.Classes
{
    public enum FilterType
    {
        EQUALS,
        LESS_THAN,
        LESS_THAN_OR_EQUAL_TO,
        GREATER_THAN,
        GREATER_THAN_OR_EQUALS_TO
    }
    public struct QueryFilter
    {
        private readonly String PropertyName;
        private readonly FilterType Operation;
        private readonly Object Value;

        public QueryFilter(String propertyName, FilterType operation, Object value)
        {
            PropertyName = propertyName;
            Operation = operation;
            Value = value;
        }

        public void Deconstruct(out String propertyName, out FilterType operation, out Object value)
        {
            propertyName = PropertyName;
            operation = Operation;
            value = Value;
        }
    }
}