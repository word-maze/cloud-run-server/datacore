using DataCore.Classes;

namespace DataCore.DataSystem
{
    /// <summary>
    /// An abstract type stored in the database.
    /// </summary>
    public abstract class DataStorable
    {
        /// <summary>
        /// The key of this item.
        /// Should refer uniquely.
        /// </summary>
        public virtual DataKey Key { get; }
    }
}