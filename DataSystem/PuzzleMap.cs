﻿using System;
using System.Collections.Generic;
using DataCore.Classes;
using DataCore.Responses;

namespace DataCore.DataSystem
{
    /// <summary>
    /// Store the non-user data for each puzzle.
    /// </summary>
    public class PuzzleMap : DataStorable
    {
        public static DataKey<PuzzleMap> CreateKey(String date, Int64 shape) => new DataKey<PuzzleMap>(date, shape.ToString());

        public override DataKey Key => CreateKey(Date, Shape);

        /// <summary>
        /// Date reference used for lookup.
        /// </summary>
        public String Date;

        /// <summary>
        /// Shape index for this puzzle.
        /// </summary>
        public Int64 Shape;

        /// <summary>
        /// The characters that appear in each position.
        /// </summary>
        public String Map;

        /// <summary>
        /// The words deliberately added at creation.
        /// Known high-point words.
        /// </summary>
        public WordData[] CreatedWords;

        /// <summary>
        /// All words found for this puzzle, store by position reference.
        /// </summary>
        public Dictionary<String, WordData> FoundWords;

        public PuzzleMap() { }
        public PuzzleMap(String date, Int64 shape)
        {
            Date = date;
            Shape = shape;
            Map = "";
            CreatedWords = new WordData[0];
            FoundWords = new Dictionary<String, WordData>();
        }

        /// <summary>
        /// Add a new word to the list of found words.
        /// </summary>
        /// <param name="word">The addition.</param>
        public void AddWord(WordData word)
        {
            String reference = Utils.PositionReference(word.Positions);

            if (!FoundWords.ContainsKey(reference))
            {
                FoundWords.Add(reference, word);
            }
        }
    }
}
