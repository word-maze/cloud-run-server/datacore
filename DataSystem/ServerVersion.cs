﻿using System;
using DataCore.Classes;

namespace DataCore.DataSystem
{
    /// <summary>
    /// Stores a version for the server.
    /// </summary>
    [Serializable]
    public class ServerVersion : DataStorable
    {
        public static DataKey<ServerVersion> CreateKey(Int64 major, Int64 minor) => new DataKey<ServerVersion>($"{major}.{minor}");
        public override DataKey Key => CreateKey(MajorVersion, MinorVersion);
        /// <summary>
        /// The major version, for api-rewrite changes.
        /// </summary>
        public Int64 MajorVersion;
        /// <summary>
        /// The minor version, for content-release changes.
        /// </summary>
        public Int64 MinorVersion;
        /// <summary>
        /// The patch version, for fixes.
        /// </summary>
        public Int64 PatchVersion;
        /// <summary>
        /// The URL that this version is accessed on.
        /// </summary>
        public String BaseURL;

        public Version Version
        {
            get => new Version((Int32)MajorVersion, (Int32)MinorVersion, (Int32)PatchVersion);
            set
            {
                MajorVersion = value.Major;
                MinorVersion = value.Minor;
                PatchVersion = value.Build;
            }
        }

        public ServerVersion() { }

        public ServerVersion(Version version, String baseURL)
        {
            Version = version;
            BaseURL = baseURL;
        }
    }
}
