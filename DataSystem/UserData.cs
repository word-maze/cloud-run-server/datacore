﻿using System;
using DataCore.Classes;

namespace DataCore.DataSystem
{
    /// <summary>
    /// Stores data about the user that isn't related to a specific puzzle.
    /// </summary>
    public class UserData : DataStorable
    {
        public static DataKey<UserData> CreateKey(String uid) => new DataKey<UserData>(uid);
        public String ID;

        public override DataKey Key => CreateKey(ID);

        /// <summary>
        /// If the user is premium, they can play any puzzle, not just the last week's.
        /// </summary>
        public Boolean Premium;

        /// <summary>
        /// How many hints they have available.
        /// </summary>
        public Int64 AvailableHints;

        /// <summary>
        /// The time this user first played.
        /// </summary>
        public DateTime Creation;

        /// <summary>
        /// The last time the user played.
        /// </summary>
        public DateTime LastInteraction;

        public UserData() {}
        public UserData(String uid)
        {
            ID = uid;
            Premium = false;
            AvailableHints = 0;
            Creation = DateTime.UtcNow;
            LastInteraction = DateTime.UtcNow;
        }
    }
}
