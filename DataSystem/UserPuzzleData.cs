﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataCore.Classes;
using DataCore.Responses;

namespace DataCore.DataSystem
{
    /// <summary>
    /// Data on a user's progress on a puzzle.
    /// </summary>
    public class UserPuzzleData : DataStorable
    {
        public static DataKey<UserPuzzleData> CreateKey(String date, Int64 shape, String uid) => new DataKey<UserPuzzleData>(date, shape.ToString(), uid);
        public String ID;
        public override DataKey Key => CreateKey(Date, Shape, ID);

        /// <summary>
        /// Date reference.
        /// </summary>
        public String Date;

        /// <summary>
        /// The shape index.
        /// </summary>
        public Int64 Shape;

        /// <summary>
        /// The words this user has found for this puzzle.
        /// </summary>
        public List<WordData> FoundWords;

        /// <summary>
        /// The hints his user has requested for this puzzle.
        /// They are provided as complete words to be found somewhere in the map.
        /// </summary>
        public List<WordData> Hints;

        public UserPuzzleData() { }
        public UserPuzzleData(String uid, String date, Int64 shape)
        {
            ID = uid;
            Date = date;
            Shape = shape;
            FoundWords = new List<WordData>();
            Hints = new List<WordData>();
        }

        /// <summary>
        /// Add a word to the list of found words.
        /// </summary>
        /// <param name="word">The addition.</param>
        public void AddWord(WordData word)
        {
            if (word.Points > 0
            && FoundWords.Any((x) => x.Positions.Equals(word.Positions)))
            {
                FoundWords.Add(word);

                Int32 hintIndex = Hints.FindIndex(x => x.Positions.Equals(word.Positions));
                if (hintIndex >= 0)
                {
                    Hints.RemoveAt(hintIndex);
                }
            }
        }

        /// <summary>
        /// Get the users total current score for this puzzle.
        /// The sum of their points for each word found.
        /// </summary>
        /// <returns>The points.</returns>
        public Int64 GetScore()
        {
            Int64 score = 0;

            for (Int32 i = 0; i < FoundWords.Count; i++)
            {
                score += FoundWords[i].Points;
            }

            return score;
        }
    }
}
