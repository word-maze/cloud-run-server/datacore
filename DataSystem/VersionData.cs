﻿using System;
using DataCore.Classes;

namespace DataCore.DataSystem
{
    [Serializable]
    public class VersionData : DataStorable
    {
        public static DataKey<VersionData> CreateKey(Version clientVersion) => new DataKey<VersionData>(clientVersion.ToString());
        public override DataKey Key => CreateKey(ClientVersion);
        /// <summary>
        /// The major client version.
        /// </summary>
        public Int32 MajorClientVersion;
        /// <summary>
        /// The minor client version.
        /// </summary>
        public Int32 MinorClientVersion;
        /// <summary>
        /// The patch client version.
        /// </summary>
        public Int32 PatchClientVersion;

        /// <summary>
        /// The single type of the client version.
        /// </summary>
        public Version ClientVersion
        {
            get => new Version(MajorClientVersion, MinorClientVersion, PatchClientVersion);
            set
            {
                MajorClientVersion = value.Major;
                MinorClientVersion = value.Minor;
                PatchClientVersion = value.Build;
            }
        }

        /// <summary>
        /// Server version as a string.
        /// </summary>
        public String ServerVersionDescription;

        /// <summary>
        /// The single type of the server version.
        /// </summary>
        public Version ServerVersion
        {
            get => Version.Parse(ServerVersionDescription);
            set => ServerVersionDescription = value.ToString();
        }

        /// <summary>
        /// The server URL that serves this client version.
        /// </summary>
        public String ServerURL;

        public VersionData() { }

        public VersionData(Version clientVersion, Version serverVersion, String serverURL)
        {
            ClientVersion = clientVersion;
            ServerVersion = serverVersion;
            ServerURL = serverURL;
        }
    }
}