﻿using System;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Responses;

namespace DataCore.Interfaces
{
    /// <summary>
    /// The API for requests for data.
    /// </summary>
    public interface IData
    {
        Task<PuzzleMap> GetPuzzleMap(String date, Int32 shape, String uid);
        Task<String> GetShapeData(Int32 shape);
        Task<FoundPuzzleWords> GetFoundWords(String date, Int32 shape, String token);
    }
}
