using System;
using System.Threading.Tasks;

namespace DataCore.Interfaces
{
    /// <summary>
    /// An interface for interacting with the database.
    /// We create a lambda that uses a transaction to load data, and returns data that needs to be set in the database.
    /// </summary>
    public interface ILoadable
    {
        /// <summary>
        /// Creates a transaction that reads from the database and writes back to it.
        /// Also, provides a return value.
        /// </summary>
        /// <typeparam name="T">The return type.</typeparam>
        /// <param name="interaction">The logic for interacting with the database, during a transaction.</param>
        /// <returns>The specified return object from the transaction.</returns>
        public Task<T> Interact<T>(Func<ITransaction, Task<T>> interaction);
        /// <summary>
        /// Creates a transaction that reads from the database and writes back to it.
        /// </summary>
        /// <param name="interaction">The logic for interacting with the database, during a transaction.</param>
        /// <returns>An empty task, to allow the interaction to be awaited.</returns>
        public Task Interact(Func<ITransaction, Task> interaction);
        /// <summary>
        /// Creates a transaction for reading from a database, but not writing to it.
        /// </summary>
        /// <typeparam name="T">The type to return.</typeparam>
        /// <param name="interaction">The logic for interacting with the database, during a transaction.</param>
        /// <returns>The specified return object from the transaction.</returns>
        public Task<T> Gather<T>(Func<ITransaction, Task<T>> interaction);
    }
}