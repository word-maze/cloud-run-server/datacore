﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataCore.DataSystem;
using DataCore.Responses;

namespace DataCore.Interfaces
{
    /// <summary>
    /// The API for action we can take that change data in the database.
    /// </summary>
    public interface IOperations
    {
        Task GenerateDailyPuzzles();
        Task<FoundWord> CheckWord(String date, Int32 shape, String uid, WordData word);
        Task<FoundPuzzleWords> BatchCheck(String date, Int32 shape, String token, WordList words);
        Task<WordList> RequestHint(String date, Int32 shape, String token, Boolean buy);
    }
}
