﻿namespace DataCore.Interfaces
{
    /// <summary>
    /// The interface for the overall server API.
    /// </summary>
    public interface IServer
    {
        /// <summary>
        /// The API for action we can take that change data in the database.
        /// </summary>
        IOperations OperationsAPI { get; }
        /// <summary>
        /// The API for requests for data.
        /// </summary>
        IData DataAPI { get; }
        /// <summary>
        /// The API for user specific, but not puzzle-specific data.
        /// </summary>
        IUser UserAPI { get; }

        IVersion VersionAPI { get; }
    }
}
