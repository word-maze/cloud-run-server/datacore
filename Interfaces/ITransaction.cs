﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataCore.Classes;
using DataCore.DataSystem;

namespace DataCore.Interfaces
{
    public interface ITransaction
    {
        /// <summary>
        /// Get an object from the database.
        /// </summary>
        /// <param name="key">The key that refers to the object you want.</param>
        /// <returns>The object you want to read.</returns>
        Task<DataStorable> Get(DataKey key);
        /// <summary>
        /// Get an object from the database.
        /// </summary>
        /// <typeparam name="T">The type you want to get.</typeparam>
        /// <param name="key">The key that refers to the object you want.</param>
        /// <returns>The object you want to read.</returns>
        Task<T> Get<T>(DataKey<T> key) where T : DataStorable, new();

        /// <summary>
        /// A query to perform on the database.
        /// </summary>
        /// <typeparam name="T">The type of the objects you want to receive.</typeparam>
        /// <param name="parameters">A set of query parameters. These are: Value name, check operation ( = < > =< => ), and the value.</param>
        /// <returns>A list of the objects that match the provided query.</returns>
        Task<List<T>> Query<T>(List<QueryFilter> parameters) where T : DataStorable, new();
        /// <summary>
        /// Get multiple objects from the database.
        /// Uses a dictionary to connect the keys to the objects they refer to.
        /// </summary>
        /// <param name="keys">The keys for the objects requested.</param>
        /// <returns>The requested objects, stored against the keys used to request them.</returns>
        Task<Dictionary<DataKey, DataStorable>> Get(params DataKey[] keys);
        /// <summary>
        /// Schedule a set operation for this object, at the end of the current transaction.
        /// </summary>
        /// <param name="data">The object we want to store.</param>
        void QueueSet(params DataStorable[] data);
        /// <summary>
        /// Schedule a set operation for these objects, at the end of the current transaction.
        /// </summary>
        /// <param name="data">The objects we want to store.</param>
        void QueueSet(IEnumerable<DataStorable> data);
        /// <summary>
        /// Schedule a delete operation for this object, at the end of the current transaction, before any set operations.
        /// </summary>
        /// <param name="data">The object we want to delete.</param>
        void QueueDelete(params DataStorable[] data);
        /// <summary>
        /// Schedule a delete operation for these objects, at the end of the current transaction, before any set operations.
        /// </summary>
        /// <param name="data">The objects we want to delete.</param>
        void QueueDelete(IEnumerable<DataStorable> data);
    }
}
