﻿using System;
using System.Threading.Tasks;
using DataCore.DataSystem;

namespace DataCore.Interfaces
{
    /// <summary>
    /// The API for user specific, but not puzzle-specific data.
    /// </summary>
    public interface IUser
    {
        Task<String> CreateNewUser();
        Task Combine(String oldToken, String newToken);
        Task Announce(String uid);
        Task<UserData> GetUserData(String uid);
    }
}
