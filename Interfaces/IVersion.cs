﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataCore.DataSystem;

namespace DataCore.Interfaces
{
    /// <summary>
    /// The API for tracking client and server versions, to ensure compatibility.
    /// </summary>
    public interface IVersion
    {
        /// <summary>
        /// For a given version of the client, get the current version of the server.
        /// </summary>
        /// <param name="clientVersion">The calling version of the client.</param>
        /// <returns></returns>
        Task<VersionData> GetAssociatedVersion(Version clientVersion);

        /// <summary>
        /// Register a new version of the server.
        /// </summary>
        /// <param name="serverVersion">The new version of the server.</param>
        /// <param name="url">The associated URL.</param>
        /// <returns></returns>
        Task Declare(Version serverVersion, String url);

        /// <summary>
        /// Connects a version of the client with a version of the server.
        /// </summary>
        /// <param name="clientVersion">The client version.</param>
        /// <param name="serverVersion">The server version.</param>
        /// <returns></returns>
        Task AssociateVersions(Version clientVersion, Version serverVersion);
    }
}
