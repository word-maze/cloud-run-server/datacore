﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataCore.Responses
{
    /// <summary>
    /// A set of words.
    /// </summary>
    public class FoundPuzzleWords
    {
        public Dictionary<String, WordData> FoundWords;

        public FoundPuzzleWords() { }

        public FoundPuzzleWords(List<WordData> words)
        {
            FoundWords = words.ToDictionary(word => word.Word);
        }
    }
}
