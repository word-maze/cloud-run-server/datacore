﻿using System;

namespace DataCore.Responses
{
    /// <summary>
    /// Represents a word & whether it has been located.
    /// </summary>
    public class FoundWord
    {
        /// <summary>
        /// The word itself.
        /// </summary>
        public String Word;
        /// <summary>
        /// The points awarded.
        /// 0 if it cannot be located in the puzzle.
        /// </summary>
        public Int64 Points;
        /// <summary>
        /// True/False if found.
        /// </summary>
        public Boolean Found;

        public FoundWord() { }

        public FoundWord(Boolean found, String word, Int64 points)
        {
            Found = found;
            Word = word;
            Points = points;
        }
    }
}
