﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DataCore.Responses
{
    /// <summary>
    /// Describes a word in a puzzle.
    /// </summary>
    public class WordData
    {
        /// <summary>
        /// The word itself.
        /// </summary>
        public String Word;
        /// <summary>
        /// The points this word is worth.
        /// </summary>
        public Int64 Points;
        /// <summary>
        /// The locations of each letter on the map.
        /// </summary>
        public Int64[] Positions;

        public WordData() { }
        public WordData(String word, Int64 points, Int64[] positions)
        {
            Word = word;
            Points = points;
            Positions = positions;
        }

        public WordData(Dictionary<String, Object> data)
        {
            Populate(data);
        }

        public void Populate(Dictionary<String, Object> data)
        {
            Word = (String)data["word"];
            Points = (Int64)data["points"];
            Positions = ((List<Object>)data["positions"]).Cast<Int64>().ToArray();
        }

        public Dictionary<String, Object> Destructure()
        {
            return new Dictionary<String, Object>
            {
                ["word"] = Word,
                ["points"] = Points,
                ["positions"] = Positions
            };
        }
    }
}