﻿using System.Collections.Generic;

namespace DataCore.Responses
{
    /// <summary>
    /// A list of words.
    /// </summary>
    public class WordList
    {
        /// <summary>
        /// The words.
        /// </summary>
        public WordData[] Words;

        public WordList() { }

        public WordList(List<WordData> words)
        {
            Words = words.ToArray();
        }
    }
}
