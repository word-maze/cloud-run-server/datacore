﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DataCore
{
    public static class Utils
    {
        public static String PositionReference(Int64[] positions)
        {
            return String.Join('-', positions);
        }

        public static T Pick<T>(this IList<T> collection, Random rng)
        {
            return collection[rng.Next(collection.Count)];
        }

        public static T Pick<T>(this T[] collection, Random rng)
        {
            return collection[rng.Next(collection.Length)];
        }
    }
}
